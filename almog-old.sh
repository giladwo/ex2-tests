#!/usr/bin/env bash

./test.sh
# for i in {0..9}; do
for i in $(seq $1 $2); do
    # diff --context=5 src/tests/almog-tabo-tests-old/game_output_${i}.txt <(build/almog-test < src/tests/almog-tabo-tests-old/game_input_${i}.txt) | bat -l diff
    # diff -s --context=5 src/tests/almog-tabo-tests-old/game_output_${i}.txt <(build/almog-test < src/tests/almog-tabo-tests-old/game_input_${i}.txt)
    # diff -y --suppress-common-lines src/tests/almog-tabo-tests-old/game_output_${i}.txt <(build/almog-test < src/tests/almog-tabo-tests-old/game_input_${i}.txt) | bat
    # diff -y -W 250 --left-column src/tests/almog-tabo-tests-old/game_output_${i}.txt <(build/almog-test < src/tests/almog-tabo-tests-old/game_input_${i}.txt) | bat
    diff -s --context=5 src/tests/almog-tabo-tests-old/game_output_${i}.txt <(build/almog-test < src/tests/almog-tabo-tests-old/game_input_${i}.txt)
done

