#ifndef SOLDIER_H
#define SOLDIER_H

#include "Character.h"

namespace mtm {
  /**
   * Describes a character of type soldier in the game.
   */
    class Soldier final : public Character {

    public:
        /**
         * Soldier- a constructor for the soldier class
         *
         * @param team - an enum indicating if the soldier is on team powerlifters or crossfitters
         * @param health - the amount of health the soldier has
         * @param ammo - the amount of ammunition the soldier has
         * @param range - the range of attack of the soldier
         * @param power - the amount of damage the soldier can deal attacking
         *
         * @return a new soldier character
         *
         */
        Soldier(Team team, units_t health, units_t ammo, units_t range, units_t power);
        /**
         * ~Soldier - Destructs a character. default destructor instead of the one it inherits.
         */
        ~Soldier() override = default;
        /**
         * operator= - a copy constructor for the soldier class
         *
         * @param other - the soldier to be copied
         *
         * @return a soldier with the same parameters as other.
         */
        Soldier& operator=(const Soldier &other);
        /**
         * clone - Duplicates a soldier (a virtual manual copy constructor).
         *
         * @return an independent duplicate of this soldier
         */
        virtual Soldier *clone() const override final;
        /**
         * swap - swaps two soldiers
         *
         * @param left - one of the soldiers to be swapped
         * @param right - one of the soldiers to be swapped
         */

        friend void swap(Soldier &left, Soldier &right) noexcept;

    protected:
        /**
         * getSymbol - Provides the soldiers symbol for board pretty-print
         *
         * @return the soldiers symbol, a char.
         */
        virtual char getSymbol() const override final;
        /**
         * getMoveRange - gets the soldiers movement range.
         *
         * @return - returns the movement range of the soldier
         */
        virtual units_t getMoveRange() const override final;
        /**
         * getReloadAmount - gets the amount of ammunition the soldier can load in a single time
         *
         * @return - the amount of ammunition
         */
        virtual units_t getReloadAmount() const override final;
        /**
         * validateEnemy - checks if the soldier can attack a character at that coordinate.
         *
         * @param difference - the location of the enemy.
         * @param target - the type of character the enemy is.
         *
         * @throws -
         * OutOfRange - if the target is out of range.
         * OutOfAmmo - if the soldier has no ammo.
         * IllegalTarget - if he cannot attack for a different reason.
         */
        virtual void validateEnemy(const GridPoint &difference,
                                   const Character *target) const override final;
        /**
         * getAttackCost - Provides the soldiers attack cost with respect to the target character
         *
         * @param enemy - the enemy that was attacked
         *
         * @return - the ammunition cost of attack.
         */
        virtual units_t getAttackCost(const Character *enemy) override final;
        /**
         * getAreaOfEffectRadius - gets the radius of AOE effects.
         *
         * @return - the AOE radius
         */
        virtual units_t getAreaOfEffectRadius() const override final;
        /**
         * impactNearbyCharacter - checks if character can be damaged by attack
         *
         * @param distance_from_target - the distance from the soldier
         * @param character - the attacked character
         *
         * @return - true if it can be damaged and false if not.
         */
        virtual bool impactNearbyCharacter(units_t distance_from_target,
                                           Character &character) const override final;
    };

    /**
     * swap - swaps two soldiers
     *
     * @param left - one of the soldiers to be swapped
     * @param right - one of the soldiers to be swapped
     */
    void swap(Soldier &left, Soldier &right) noexcept;
}


#endif // SOLDIER_H
