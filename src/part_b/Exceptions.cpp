#include "Exceptions.h"

#define IMPLEMENT_GAME_EXCEPTION(name) \
    const char* mtm::name::what() const noexcept { \
        return "A game related error has occurred: " #name; \
    }


IMPLEMENT_GAME_EXCEPTION(IllegalArgument)
IMPLEMENT_GAME_EXCEPTION(IllegalCell)
IMPLEMENT_GAME_EXCEPTION(CellEmpty)
IMPLEMENT_GAME_EXCEPTION(MoveTooFar)
IMPLEMENT_GAME_EXCEPTION(CellOccupied)
IMPLEMENT_GAME_EXCEPTION(OutOfRange)
IMPLEMENT_GAME_EXCEPTION(OutOfAmmo)
IMPLEMENT_GAME_EXCEPTION(IllegalTarget)
