#ifndef SNIPER_H
#define SNIPER_H

#include "Character.h"

namespace mtm {
    /**
     * Describes a character of type sniper in the game.
     */
    class Sniper final : public Character {

    public:
        /**
         * Sniper - a constructor for sniper.
         *
         * @param team - the team to which he belongs
         * @param health - the amount of health he has
         * @param ammo - the amount of ammunition he has
         * @param range - the amount of range of attack he has
         * @param power - the amount of damage he can inflict
         *
         * @return - a sniper with the provided parameters
         */
        Sniper(Team team, units_t health, units_t ammo, units_t range, units_t power);
        /**
         * ~Sniper - a default destructor for sniper.
         */
        ~Sniper() override = default;
        /**
         * operator= - a copy constructor for sniper
         *
         * @param other - the sniper to be copied
         *
         * @return - the copy of the provided sniper
         */
        Sniper& operator=(const Sniper& other);
        /**
         * clone - Duplicates a sniper (a virtual manual copy constructor).
         *
         * @return an independent duplicate of this sniper
         */
        virtual Sniper *clone() const override final;

        /**
        * swap - swaps two snipers
        *
        * @param left - one of the snipers to be swapped
        * @param right - one of the snipers to be swapped
        */
        friend void swap(Sniper &left, Sniper &right) noexcept;

    protected:
        /**
         * getSymbol - Provides the snipers symbol for board pretty-print
         *
         * @return the snipers symbol, a char.
         */
        virtual char getSymbol() const override final;
        /**
         * getMoveRange - gets the snipers movement range.
         *
         * @return - returns the movement range of the sniper
         */
        virtual units_t getMoveRange() const override final;
        /**
         * getReloadAmount - gets the amount of ammunition the sniper can load in a single time
         *
         * @return - the amount of ammunition
         */
        virtual units_t getReloadAmount() const override final;
        /**
        * validateEnemy - checks if the sniper can attack a character at that coordinate.
        *
        * @param difference - the location of the enemy.
        * @param target - the type of character the enemy is.
        *
        * @throws -
        * OutOfRange - if the target is out of range.
        * OutOfAmmo - if the sniper has no ammo.
        * IllegalTarget - if he cannot attack for a different reason.
        */
        virtual void validateEnemy(const GridPoint &difference,
                                   const Character *target) const override final;
        /**
         * getAttackCost - Provides the sniper attack cost with respect to the target character
         *
         * @param enemy - the enemy that was attacked
         *
         * @return - the ammunition cost of attack.
         */
        virtual units_t getAttackCost(const Character *enemy) override final;
        /**
         * getAreaOfEffectRadius - gets the radius of AOE effects.
         *
         * @return - the AOE radius
         */
        virtual units_t getAreaOfEffectRadius() const override final;
        /**
         * impactNearbyCharacter - checks if character can be damaged by attack
         *
         * @param distance_from_target - the distance from the sniper
         * @param character - the attacked character
         *
         * @return - true if it can be damaged and false if not.
         */
        virtual bool impactNearbyCharacter(units_t distance_from_target,
                                           Character &character) const override final;

    private:
        // the streak bonus relevant to rules applying only to sniper
        int getStreakBonus() const;
        // the amount needed for a streak
        static const int STREAK_GOAL = 3;
        // the factor by which the combo increases stats
        static const int COMBO_FACTOR = 2;

        int streak = 1;
    };

    /**
     * swap - swaps two snipers
     *
     * @param left - one of the snipers to be swapped
     * @param right - one of the snipers to be swapped
     */
    void swap(Sniper &left, Sniper &right) noexcept;
}


#endif // SNIPER_H
