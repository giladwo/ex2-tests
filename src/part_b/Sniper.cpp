#include <cassert>

#include "Exceptions.h"
#include "Sniper.h"

mtm::Sniper::Sniper(const Team team, const units_t health,
                    const units_t ammo, const units_t range,
                    const units_t power)
        : Character{team, health, ammo, range, power}
{

}

mtm::Sniper *mtm::Sniper::clone() const
{
    return new Sniper{*this};
}

char mtm::Sniper::getSymbol() const
{
    return 'N';
}

mtm::units_t mtm::Sniper::getMoveRange() const
{
    return 4;
}

mtm::units_t mtm::Sniper::getReloadAmount() const
{
    return 2;
}

void mtm::Sniper::validateEnemy(const GridPoint &difference,
                                const Character *const character) const
{
    assert((0 == norm(difference)) ^ (this != character));
    const units_t range = getRange();
    const units_t distance = norm(difference);
    if ((range < distance) || (ceil(range / 2.0) > distance)) {
        throw OutOfRange{};
    }
    if (!hasReserveAmmo()) {
        throw OutOfAmmo{};
    }
    if ((nullptr == character) || !isEnemy(*character)) {
        throw IllegalTarget{};
    }
}

mtm::units_t mtm::Sniper::getAttackCost(const Character *const enemy)
{
    assert(nullptr != enemy);
    // TODO make sure this is called exactly once per attack after success
    ++streak;
    return 1;
}

bool mtm::Sniper::impactNearbyCharacter(units_t distance_from_target, Character &character) const
{
    assert(isEnemy(character));
    assert(0 == distance_from_target);
    return character.hit(getStreakBonus() * getPower());
}

mtm::units_t mtm::Sniper::getAreaOfEffectRadius() const
{
    return 0;
}

int mtm::Sniper::getStreakBonus() const
{
    assert(0 < streak);
    return streak % STREAK_GOAL ? 1 : COMBO_FACTOR;
}

void mtm::swap(Sniper &left, Sniper &right) noexcept
{
    using std::swap;

    swap(static_cast<Character&>(left), static_cast<Character&>(right));
    swap(left.streak, right.streak);
}

mtm::Sniper &mtm::Sniper::operator=(const Sniper &other)
{
    Sniper copy{other};
    swap(*this, copy);
    return *this;
}
