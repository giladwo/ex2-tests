#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>

#include <cassert>

#include "Exceptions.h"
#include "Character.h"

namespace mtm {
    /// Makes sure value is valid according to Validator (throws mtm::IllegalArgument otherwise)
    template <typename Validator = std::less_equal<mtm::units_t>>
    static mtm::units_t validateUnit(mtm::units_t value);
}

mtm::Character::Character(const Team team, const units_t health,
                          const units_t ammo, const units_t range,
                          const units_t power)
        : team{team}
        , health{validateUnit<std::less<units_t>>(health)}
        , ammo{validateUnit(ammo)}
        , range{validateUnit(range)}
        , power{validateUnit(power)}
{

}

std::ostream &mtm::operator<<(std::ostream &out, const Character &character)
{
    int (*capitalize)(int) = Team::POWERLIFTERS == character.team ? toupper : tolower;
    out << static_cast<char>(capitalize(character.getSymbol()));
    return out;
}

void mtm::swap(Character &left, Character &right) noexcept
{
    using std::swap;

    swap(left.team, right.team);
    swap(left.health, right.health);
    swap(left.ammo, right.ammo);
    swap(left.range, right.range);
    swap(left.power, right.power);
}

bool mtm::Character::isEnemy(const Character &other) const
{
    return team != other.team;
}

void mtm::Character::reload()
{
    ammo += getReloadAmount();
}

const mtm::GridPoint &mtm::Character::validateMove(const GridPoint &move) const
{
    if (getMoveRange() < norm(move)) {
        throw MoveTooFar{};
    }
    return move;
}

bool mtm::Character::hit(units_t damage)
{
    // make sure this is either a proper hit or a test to make sure the character is already dead
    assert(((0 < health) && (0 < damage)) ^ ((0 == damage) && (0 >= health)));
    health -= damage;
    return 0 >= health;
}

void mtm::Character::heal(units_t amount)
{
    assert(0 < amount);
    assert(0 < health);
    health += amount;
}

mtm::units_t mtm::Character::getPower() const
{
    return power;
}

mtm::units_t mtm::Character::getRange() const
{
    return range;
}

bool mtm::Character::hasReserveAmmo() const
{
    assert(0 <= ammo);
    // assuming all attacks require a single bullet
    return 0 < ammo;
}

void mtm::Character::consumeAmmo(units_t amount)
{
    assert(0 <= ammo);
    assert(0 <= amount);
    ammo -= amount;
    assert(0 <= ammo);
}

mtm::GridPoint mtm::operator-(const GridPoint &point)
{
    return GridPoint{-point.row, -point.col};
}

mtm::GridPoint mtm::operator+(const GridPoint &left, const GridPoint &right)
{
    return GridPoint{left.row + right.row, left.col + right.col};
}

mtm::GridPoint mtm::operator-(const GridPoint &left, const GridPoint &right)
{
    return left + (-right);
}

mtm::units_t mtm::norm(const GridPoint &point)
{
    return GridPoint::distance(point, GridPoint{0, 0});
}

template <typename Validator>
static mtm::units_t mtm::validateUnit(units_t value)
{
    if (Validator{}(0, value)) {
        return value;
    }
    throw IllegalArgument{};
}

mtm::Character::InAreaOfEffect::InAreaOfEffect(const Character &character,
                                               const GridPoint &target)
    : character{&character}, target{&target}
{

}

bool mtm::Character::InAreaOfEffect::operator()(const Cell &pair) const
{
    return character->getAreaOfEffectRadius() >= norm(pair.first - *target);
}
