#ifndef GAME_H
#define GAME_H

#include <memory>
#include <iosfwd>
#include <map>

#include "Auxiliaries.h"
#include "Exceptions.h"
#include "Character.h"

namespace mtm {
    /**
     * Describes a competition between the Crossfitters and the Powerlifters
     * according to the exercise's requirements.
     */
    class Game final {
    public:
        /**
         * No default constructor (a Game without a concrete board size is not required).
         */
        Game() = delete;

        /**
         * Constructs a new Game with the given dimensions.
         *
         * @param height the board's size
         * @param width the board's width
         */
        Game(int height, int width);

        /**
         * Destructs a Game (nested RAII takes care of all allocated resources).
         */
        ~Game() = default;

        /**
         * Copy-constructs a Game - a deep copy of all characters on the other's board.
         *
         * @param other the board to copy
         */
        Game(const Game &other);

        /**
         * Copy-assigns a Game - a deep copy of all characters on the other's board.
         *
         * @param other the board to copy
         * @return a reference to the assigned-to board
         */
        Game &operator=(const Game &other);

        /**
         * Adds a Character to the Game's board (with shared ownership).
         *
         * @param coordinates the location to insert the character to
         * @param character the character to insert
         *
         * @throws IllegalCell if the coordinates are outside the board's dimensions
         * @throws CellOccupied if the coordinates' cell is already occupied
         */
        void addCharacter(const GridPoint &coordinates, std::shared_ptr<Character> character);

        /**
         * Creates a new Character (with shared ownership).
         * @related Character::Character
         *
         * @param type the type of the character to create (must be a valid CharacterType)
         * @param team the team of the character to create (must be a valid Team)
         * @param health the health of the character to create (must be positive)
         * @param ammo the ammo of the character to create (must be non-negative)
         * @param range the range of the character to create (must be non-negative)
         * @param power the power of the character to create (must be non-negative)
         * @return a new (shared) character
         *
         * @throws IllegalArgument if any of {health, ammo, range, power} is negative
         *                          or if health is 0
         */
        static std::shared_ptr<Character> makeCharacter(CharacterType type, Team team,
                                                        units_t health, units_t ammo,
                                                        units_t range, units_t power);

        /**
         * Moves a character on the board.
         *
         * @param src_coordinates the coordinates of the character to move
         * @param dst_coordinates the coordinates to move the character to
         *
         * @throws IllegalCell if either source or destination coordinates are
         *                      outside the board's bounds
         * @throws CellEmpty if no character is on the source coordinates
         * @throws MoveTooFar if the character cannot move to the supplied destination
         * @throws CellOccupied if there's already a character in the destination
         */
        void move(const GridPoint &src_coordinates, const GridPoint &dst_coordinates);

        /**
         * Attacks the destination using the character at the source.
         * Side effects: collateral damage and dead characters removal.
         *
         * @param src_coordinates the character to attack with
         * @param dst_coordinates the character to attack
         *
         * @throws IllegalCell if either source or destination coordinates are
         *                      outside the board's bounds
         * @throws CellEmpty if either source or destination coordinates are empty
         * @throws OutOfRange if the destination is out of the attacker's range
         * @throws OutOfAmmo if the attacker has no available ammo reserves
         * @throws IllegalTarget if the attacker cannot attack the character at
         *                          destination (according to the game's rules)
         */
        void attack(const GridPoint &src_coordinates, const GridPoint &dst_coordinates);

        /**
         * Replenishes a character's ammo reserves.
         *
         * @param coordinates the character to replenish the ammo of
         *
         * @throws IllegalCell if the coordinates are outside the board's bounds
         * @throws CellEmpty if there's no character at the supplied coordinates
         */
        void reload(const GridPoint &coordinates);

        /**
         * Formats the board to the output stream.
         *
         * @param os the output stream to write to
         * @param game the game to format the board of
         * @return the used output stream
         */
        friend std::ostream &operator<<(std::ostream &os, const Game &game);

        /**
         * Checks if there's a winning team:
         *      1. the board is not empty, and
         *      2. all characters are of the same team
         *
         * @param winningTeam if valid and there's a winner, it's set to the winner
         * @return true if there's a winner
         */
        bool isOver(Team *winningTeam=NULL) const;

    private:
        /// A helper type to make signatures more readable
        using Board = std::map<const GridPoint, std::shared_ptr<Character>>;

        /**
         * Used to filter out board cells that hold characters of the same team.
         */
        class IsEnemyOf final {
        public:
            /**
             * Constructs an IsEnemyOf predicate.
             *
             * @param character the character to use to identify enemies.
             */
            IsEnemyOf(const Character &character);

            /**
             * Destructs an IsEnemyOf (owns no resources).
             */
            ~IsEnemyOf() = default;

            /**
             * Prevent assignment (not needed).
             */
            IsEnemyOf &operator=(const IsEnemyOf &other) = delete;

            /**
             * Determines if the cell holds an enemy character.
             *
             * @param pair the board cell to check
             * @return true if the cell contains an enemy character
             */
            bool operator()(Board::const_reference pair) const;

        private:
            /// The character to use
            const Character * const character;
        };

        /**
         * Used to clean up dead characters.
         */
        class RemoveDead final {
        public:
            /**
             * Constructs a RemoveDead tied to a Board.
             *
             * @param characters the board to remove characters from
             */
            RemoveDead(Board &characters);

            /**
             * Destructs a RemoveDead (owns no resources).
             */
            ~RemoveDead() = default;

            /**
             * Prevent assignment (not needed).
             */
            RemoveDead &operator=(const RemoveDead &other) = delete;

            /**
             * Removes the (assumed to be dead) character at the cell
             * pointed to by the iterator and returns an iterator
             * pointing to the next cell.
             *
             * @param iterator the cell to remove the character at
             * @return iterator to the next cell
             */
            Board::iterator operator()(Board::iterator iterator) const;

        private:
            /// The board to remove characters from
            Board * const characters;
        };

        /// Swaps two boards
        void swap(Game &other) noexcept;

        /// Finds a character
        std::shared_ptr<Character> find(const GridPoint &coordinates) const;

        /// Makes sure the coordinate is inside the board's bounds
        const GridPoint &validateBounds(const GridPoint &coordinates) const;

        /// Makes sure the coordinate's cell isn't empty
        const GridPoint &validateEmpty(const GridPoint &coordinates) const;

        /// The vertical board dimension
        int height;

        /// The horizontal board dimension
        int width;

        /// The board that owns the characters
        Board characters;
    };

    /// A forward declaration of the output operator (see friend declaration inside Game)
    std::ostream &operator<<(std::ostream &os, const Game &game);

    // for Board (std::map with GridPoint keys)
    /// Compares two GridPoints lexicographically (by row then by column)
    bool operator<(const GridPoint &left, const GridPoint &right);
}

#endif // GAME_H
