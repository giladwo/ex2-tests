#include <cassert>

#include "Medic.h"

mtm::Medic::Medic(const Team team, const units_t health,
                  const units_t ammo, const units_t range,
                  const units_t power)
        : Character{team, health, ammo, range, power}
{

}

char mtm::Medic::getSymbol() const
{
    return 'M';
}

mtm::Medic *mtm::Medic::clone() const
{
    return new Medic{*this};
}

mtm::units_t mtm::Medic::getMoveRange() const
{
    return 5;
}

mtm::units_t mtm::Medic::getReloadAmount() const
{
    return 5;
}

void mtm::Medic::validateEnemy(const GridPoint &difference,
                               const Character *const character) const
{
    assert((0 == norm(difference)) ^ (this != character));
    if (getRange() < norm(difference)) {
        throw OutOfRange{};
    }
    if ((nullptr == character) || (this == character)) {
        throw IllegalTarget{};
    }
    assert(nullptr != character);
    if (isEnemy(*character) && !hasReserveAmmo()) {
        throw OutOfAmmo{};
    }
}

mtm::units_t mtm::Medic::getAttackCost(const Character *const enemy)
{
    assert(nullptr != enemy);
    return isEnemy(*enemy);
}

mtm::units_t mtm::Medic::getAreaOfEffectRadius() const
{
    return 0;
}

bool mtm::Medic::impactNearbyCharacter(units_t distance_from_target, Character &character) const
{
    assert(0 == distance_from_target);
    if (isEnemy(character)) {
        return character.hit(getPower());
    } else {
        character.heal(getPower());
        return false;
    }
}

void mtm::swap(Medic &left, Medic &right) noexcept
{
    using std::swap;

    swap(static_cast<Character&>(left), static_cast<Character&>(right));
}

mtm::Medic &mtm::Medic::operator=(const Medic &other)
{
    Medic copy{other};
    swap(*this, copy);
    return *this;
}
