### Dry: `SortedList` questions

#### Questions
1. What requirements does `SortedList<T>` impose on `T`?
1. Why is a non-const `SortedList<T>::iterator` problematic?
1. `SortedList<T>::filter` uses an additional template parameter for the predicate.

   Name two other ways to accomplish this and explain the difference between them.

#### Answers
1. `T` must provide
   1. A copy constructor - to create independent copies of the inserted objects.
      * A move constructor (used instead of the copy constructor on prvalues and
        <br/> compilation fails if it's explicitly deleted).
   1. A comparison operation (we chose `operator<`)` - to sort the list with.
1. Providing mutable access to the elements (a non-const reference) means
   <br/> means it's easy to invalidate the internal order (an element may
   <br/> change and be out of place). Thus in order to support a non-const
   <br/> iterator the list would have to keep track of changes performed
   <br/> through iterators and constantly reorder itself, which requires
   <br/> invalidation of all existing iterators and generally creates a lot
   <br/> of room for bugs.
1. We can use
   1. function pointers (C style)
   2. function objects (instances of a class that implement a suitable `operator()`)

   We prefer function objects because they're more versatile (i.e. can trivially
   <br/> be initialized with arguments, obviating the need to pass additional
   <br/> parameters manually, can be inlined more easily (especially with lambdas)).

   Using a template, the compiler generates implementations for any suitable
   <br/> argument that's actually used (and methods that aren't used aren't compiled
   <br/> at all)
