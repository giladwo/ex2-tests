#include <gtest/gtest.h>

#define protected public
#define private public
#include "../part_b/Medic.h"
#undef private
#undef protected

class MedicFixture: public ::testing::Test {
public:
    MedicFixture() : medic{TEAM, HEALTH, AMMO, RANGE, POWER} {}
    virtual ~MedicFixture() = default;

    void SetUp() override {
        medic = mtm::Medic{TEAM, HEALTH, AMMO, RANGE, POWER};
    }

    void TearDown() override {
    }

    static const mtm::Team TEAM = mtm::Team::POWERLIFTERS;
    static const mtm::units_t HEALTH = 3;
    static const mtm::units_t AMMO = 3;
    static const mtm::units_t RANGE = 3;
    static const mtm::units_t POWER = 3;
    mtm::Medic medic;
};

TEST_F(MedicFixture, VerifyBasicStats) {
    EXPECT_EQ(5, medic.getMoveRange());
    EXPECT_EQ(5, medic.getReloadAmount());
    EXPECT_EQ(0, medic.getAttackCost(&medic));
    auto enemy{medic};
    enemy.team = (mtm::Team::POWERLIFTERS == medic.team) ? mtm::Team::CROSSFITTERS : mtm::Team::POWERLIFTERS;
    EXPECT_EQ(1, medic.getAttackCost(&enemy));
    EXPECT_EQ(0, medic.getAreaOfEffectRadius());
}

TEST(DISABLED_MedicFixture, CannotAttackItself) {
//    EXPECT_TRUE(medic.isInRange(mtm::GridPoint{0, 0}));
//    EXPECT_FALSE(medic.validateEnemy(mtm::GridPoint{0, 1}, nullptr));
//    EXPECT_FALSE(medic.validateEnemy(mtm::GridPoint{0, 1}, NULL));
//    EXPECT_FALSE(medic.validateEnemy(mtm::GridPoint{0, 0}, &medic));
}
