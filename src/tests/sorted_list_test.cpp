#include <iosfwd>
#include <algorithm>
#include <gtest/gtest.h>

#define protected public
#define private public
#include "../part_a/sortedList.h"
#undef private
#undef protected

using std::cout;
using std::endl;
using std::ostringstream;
using std::string;
using mtm::SortedList;

string getLen(const string& str) {
    return std::to_string(str.length());
}

template <typename T>
void printList(const SortedList<T>& list, ostringstream& out) {
    for (auto it = list.begin(); !(it == list.end()); ++it) {
        out << *it << endl;
    }
    out << endl;
}

TEST(SortedList, ExampleSnippet1) {
    ostringstream out{};
    SortedList<string> lst1 = SortedList<string>();
    lst1.insert("Charlie");
    lst1.insert("Bob");
    lst1.insert("Alice");
    lst1.insert("Donald");
    printList(lst1, out);

    SortedList<string> lst2 = lst1;
    lst2 = lst2.apply(getLen);
    printList(lst2, out);

    SortedList<string>::const_iterator it = lst2.begin();
    out << *it << endl << endl;
    ++it;
    lst2.remove(it);
    printList(lst2, out);

    const char * const expected =
            "Alice\n"
            "Bob\n"
            "Charlie\n"
            "Donald\n"
            "\n"
            "3\n"
            "5\n"
            "6\n"
            "7\n"
            "\n"
            "3\n"
            "\n"
            "3\n"
            "6\n"
            "7\n"
            "\n";
    EXPECT_STREQ(expected, out.str().c_str());
}

TEST(SortedList, VerifyInternalOrder) {
    SortedList<int> list;
    int const repetitions = 30;
    std::srand(std::time(nullptr));
    for (int i = 0; i < repetitions; ++i) {
        list.insert(std::rand() % 100);
    }
    EXPECT_TRUE(std::is_sorted(std::begin(list), std::end(list)));
}

TEST(SortedList, OutOfBounds) {
    SortedList<int> list;
    EXPECT_EQ(std::end(list), std::begin(list));
    EXPECT_THROW(list.remove(std::end(list)), std::out_of_range);
    auto iterator{std::end(list)};
    EXPECT_THROW(*iterator, std::out_of_range);
    EXPECT_THROW(++iterator, std::out_of_range);
    EXPECT_THROW(iterator++, std::out_of_range);
}

TEST(SortedList, TestCopyConstructor) {
    SortedList<int> list;
    list.insert(10);
    list.insert(5);
    list.insert(0);
    SortedList<int> list2{list};
    EXPECT_TRUE(std::is_sorted(std::begin(list), std::end(list)));
}

TEST(SortedList, TestAssignment) {
    SortedList<int> list;
    list.insert(10);
    list.insert(5);
    list.insert(0);
    SortedList<int> list2;
    list2 = list;
    EXPECT_TRUE(std::is_sorted(std::begin(list), std::end(list)));
    EXPECT_TRUE(std::is_sorted(std::begin(list2), std::end(list2)));
}

TEST(SortedList, TestRemove) {
    SortedList<int> list;
    list.insert(1);
    list.insert(2);
    list.insert(-2);
    list.remove(++list.begin());
    EXPECT_EQ(2, list.length());
    auto iterator = list.begin();
    EXPECT_EQ(-2, *iterator);
    ++iterator;
    EXPECT_EQ(2, *iterator);
    ++iterator;
    EXPECT_EQ(list.end(), iterator);
}

class SortedListMinimumRequirements {
public:
    SortedListMinimumRequirements() = delete;
    ~SortedListMinimumRequirements() = default;
    SortedListMinimumRequirements(const SortedListMinimumRequirements &other) = default;
    SortedListMinimumRequirements &operator=(const SortedListMinimumRequirements &other) = delete;
    SortedListMinimumRequirements(SortedListMinimumRequirements &&other) = default;
    SortedListMinimumRequirements &operator=(const SortedListMinimumRequirements &&other) = delete;
//    bool operator<(const SortedListMinimumRequirements &other) const {
//        return false;
//    }
};

bool operator<(const SortedListMinimumRequirements &left, const SortedListMinimumRequirements &right) {
    return false;
}

TEST(SortedList, Requirements) {
    SortedList<SortedListMinimumRequirements> list;
    EXPECT_EQ(0, list.length());
    EXPECT_EQ(std::begin(list), std::end(list));
    list.insert(SortedListMinimumRequirements{});
    EXPECT_EQ(1, list.length());
    EXPECT_NE(std::begin(list), std::end(list));
    for (auto &&x : list) {
        EXPECT_FALSE(x < x);
    }

    SortedList<SortedListMinimumRequirements> duplicate{list};
    SortedList<SortedListMinimumRequirements> duplicate2 = list.apply([](const SortedListMinimumRequirements &x) { return x; });
    SortedList<SortedListMinimumRequirements> duplicate3 = list.filter([](const SortedListMinimumRequirements &x) { return true; });
    SortedList<SortedListMinimumRequirements> empty = list.filter([](const SortedListMinimumRequirements &x) { return false; });
    // TODO add more assertions here

    SortedList<SortedListMinimumRequirements> assigned_to;
    assigned_to = list;

    list.clear();
    EXPECT_EQ(0, list.length());
    EXPECT_EQ(std::begin(list), std::end(list));
}

TEST(SortedList, CopyOnInsert) {
    SortedList<int> list;
    int x = 0;
    list.insert(x);
    ASSERT_NE(&x, &*std::begin(list));
}

TEST(SortedList, CopyConstructorCopiesElementValues) {
    SortedList<int> list;
    int a = 1;
    int b = -1;
    int c = 3;
    list.insert(a);
    list.insert(b);
    list.insert(c);
    SortedList<int> copy{list};
    EXPECT_EQ(list.length(), copy.length());

    auto original = std::begin(list);
    auto clone = std::begin(copy);
    for (int i = 0; i < list.length(); ++i) {
        EXPECT_NE(&*original, &*clone);
        ++original;
        ++clone;
    }
    EXPECT_EQ(std::end(list), original);
    EXPECT_EQ(std::end(copy), clone);
}
