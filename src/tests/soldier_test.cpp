#include <gtest/gtest.h>

#define protected public
#define private public
#include "../part_b/Soldier.h"
#undef private
#undef protected

class SoldierFixture: public ::testing::Test {
public:
    SoldierFixture() : soldier{TEAM, HEALTH, AMMO, RANGE, POWER} {}
    virtual ~SoldierFixture() = default;

    void SetUp() override {
        soldier = mtm::Soldier{TEAM, HEALTH, AMMO, RANGE, POWER};
    }

    void TearDown() override {
    }

    static const mtm::Team TEAM = mtm::Team::POWERLIFTERS;
    static const mtm::units_t HEALTH = 3;
    static const mtm::units_t AMMO = 3;
    static const mtm::units_t RANGE = 3;
    static const mtm::units_t POWER = 3;
    mtm::Soldier soldier;
};

TEST_F(SoldierFixture, VerifyBasicStats) {
    EXPECT_EQ(std::ceil(soldier.getRange() / 3), soldier.getAreaOfEffectRadius());
    EXPECT_EQ(1, soldier.getAttackCost(nullptr));
    EXPECT_EQ(1, soldier.getAttackCost(&soldier));
    auto enemy{soldier};
    enemy.team = (mtm::Team::POWERLIFTERS == soldier.team) ? mtm::Team::CROSSFITTERS : mtm::Team::POWERLIFTERS;
    EXPECT_EQ(1, soldier.getAttackCost(&enemy));
    EXPECT_EQ(3, soldier.getReloadAmount());
    EXPECT_EQ(3, soldier.getMoveRange());
}
