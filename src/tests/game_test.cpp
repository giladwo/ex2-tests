#include <gtest/gtest.h>

#include "../part_b/Auxiliaries.h"

#define protected public
#define private public
#include "../part_b/Game.h"
#undef private
#undef protected

class GameFixture: public ::testing::Test {
public:
    // Initialization
    GameFixture() : game{HEIGHT, WIDTH} { }

    // Teardown
    virtual ~GameFixture() = default;

    // Per-test setup
    void SetUp() override {
        game = mtm::Game{HEIGHT, WIDTH};
        soldier = game.makeCharacter(mtm::CharacterType::SOLDIER, mtm::Team::CROSSFITTERS, 1, 2, 3, 4);
        game.addCharacter(mtm::GridPoint{1, 1}, soldier);
        sniper = game.makeCharacter(mtm::CharacterType::SNIPER, mtm::Team::CROSSFITTERS, 1, 2, 3, 4);
        game.addCharacter(mtm::GridPoint{1, 2}, sniper);
    }

    // Per-test cleanup
    void TearDown() override {
    }

    static const int HEIGHT = 10;
    static const int WIDTH = 20;
    mtm::Game game;
    std::shared_ptr<mtm::Character> sniper;
    std::shared_ptr<mtm::Character> soldier;
};

TEST_F(GameFixture, CellIsAlreadyTaken) {
    EXPECT_THROW(game.addCharacter(mtm::GridPoint{1,1}, std::shared_ptr<mtm::Character>()), mtm::CellOccupied);
}

TEST(Game, GameIllegalArgument) {
    EXPECT_THROW((mtm::Game{0,3}), mtm::IllegalArgument);
    EXPECT_THROW((mtm::Game{1,-2}), mtm::IllegalArgument);
}

TEST_F(GameFixture, IllegalCell) {
    std::shared_ptr<mtm::Character> character = game.makeCharacter(mtm::CharacterType::SOLDIER, mtm::Team::CROSSFITTERS, 1, 2, 3, 4);
    EXPECT_THROW(game.addCharacter(mtm::GridPoint{15,17}, character), mtm::IllegalCell);
    EXPECT_THROW(game.addCharacter(mtm::GridPoint{7,-3}, character), mtm::IllegalCell);
}

TEST_F(GameFixture, makeCharacterExceptions) {
    EXPECT_THROW(game.makeCharacter(mtm::CharacterType::SOLDIER, mtm::Team::POWERLIFTERS, 0, 3, 2, 3), mtm::IllegalArgument);
    EXPECT_THROW(game.makeCharacter(mtm::CharacterType::SNIPER, mtm::Team::CROSSFITTERS, 1, -3, 2, 3), mtm::IllegalArgument);
    EXPECT_THROW(game.makeCharacter(mtm::CharacterType::MEDIC, mtm::Team::POWERLIFTERS, 2, 3, -2, 3), mtm::IllegalArgument);
    EXPECT_THROW(game.makeCharacter(mtm::CharacterType::SOLDIER, mtm::Team::POWERLIFTERS, 2, 3, 2, -3), mtm::IllegalArgument);
}

TEST_F(GameFixture, moveExceptions) {
    EXPECT_THROW(game.move(mtm::GridPoint{-2,5}, mtm::GridPoint{7,13}), mtm::IllegalCell);
    EXPECT_THROW(game.move(mtm::GridPoint{4,5}, mtm::GridPoint{1,1}), mtm::CellEmpty);
    EXPECT_THROW(game.move(mtm::GridPoint{1,1}, mtm::GridPoint{7,13}), mtm::MoveTooFar);
    EXPECT_THROW(game.move(mtm::GridPoint{1,1}, mtm::GridPoint{1,2}), mtm::CellOccupied);
}

TEST_F(GameFixture, attackExceptions) {
    EXPECT_THROW(game.attack(mtm::GridPoint{-2,5}, mtm::GridPoint{7,13}), mtm::IllegalCell);
    EXPECT_THROW(game.attack(mtm::GridPoint{3,5}, mtm::GridPoint{7,13}), mtm::CellEmpty);
    std::shared_ptr<mtm::Character> no_ammo_soldier = game.makeCharacter(mtm::CharacterType::SOLDIER, mtm::Team::CROSSFITTERS, 1, 0, 1, 4);
    game.addCharacter(mtm::GridPoint{2, 2}, no_ammo_soldier);
    EXPECT_THROW(game.attack(mtm::GridPoint{2,2}, mtm::GridPoint{1,1}), mtm::OutOfRange);
    EXPECT_THROW(game.attack(mtm::GridPoint{2,2}, mtm::GridPoint{1,2}), mtm::OutOfAmmo);
    EXPECT_THROW(game.attack(mtm::GridPoint{1,2}, mtm::GridPoint{1,3}), mtm::OutOfRange);
    EXPECT_THROW(game.attack(mtm::GridPoint{1,2}, mtm::GridPoint{1,4}), mtm::IllegalTarget);
}

TEST_F(GameFixture, reloadExceptions) {
    EXPECT_THROW(game.reload(mtm::GridPoint{-2,3}), mtm::IllegalCell);
    EXPECT_THROW(game.reload(mtm::GridPoint{7,7}), mtm::CellEmpty);
}

TEST(Game, CopyClonesCharacters) {
    mtm::Game game{3, 3};
    const auto character = game.makeCharacter(mtm::CharacterType::SOLDIER, mtm::Team::CROSSFITTERS, 1, 1, 1, 1);
    const auto location = mtm::GridPoint{0, 0};
    game.addCharacter(location, character);
    EXPECT_EQ(character, game.characters.at(location));
    mtm::Game duplicate{game};
    EXPECT_EQ(character, game.characters.at(location));
    EXPECT_NE(duplicate.characters.at(location).get(), character.get());
}

TEST_F(GameFixture, EmptyGameIsNotOver) {
    mtm::Game empty{1, 2};
    EXPECT_FALSE(empty.isOver());
    EXPECT_FALSE(empty.isOver(nullptr));
    EXPECT_FALSE(empty.isOver(NULL));
    mtm::Team winner = mtm::Team::POWERLIFTERS;
    EXPECT_FALSE(empty.isOver(&winner));
    EXPECT_EQ(mtm::Team::POWERLIFTERS, winner);

    empty.addCharacter(mtm::GridPoint{0, 0}, sniper);
    EXPECT_TRUE(empty.isOver());
    EXPECT_TRUE(empty.isOver(nullptr));
    EXPECT_TRUE(empty.isOver(NULL));
    EXPECT_TRUE(empty.isOver(&winner));
    EXPECT_EQ(mtm::Team::CROSSFITTERS, winner);
}
