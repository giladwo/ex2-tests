#include <gtest/gtest.h>

#define protected public
#define private public
#include "../part_b/Sniper.h"
#undef private
#undef protected

class SniperFixture: public ::testing::Test {
public:
    SniperFixture() : sniper{TEAM, HEALTH, AMMO, RANGE, POWER} {}
    virtual ~SniperFixture() = default;

    void SetUp() override {
        sniper = mtm::Sniper{TEAM, HEALTH, AMMO, RANGE, POWER};
    }

    void TearDown() override {
    }

    static const mtm::Team TEAM = mtm::Team::POWERLIFTERS;
    static const mtm::units_t HEALTH = 3;
    static const mtm::units_t AMMO = 3;
    static const mtm::units_t RANGE = 3;
    static const mtm::units_t POWER = 3;
    mtm::Sniper sniper;
};

TEST_F(SniperFixture, VerifyBasicStats) {
    EXPECT_EQ(4, sniper.getMoveRange());
    EXPECT_EQ(2, sniper.getReloadAmount());
    auto enemy{sniper};
    enemy.team = (mtm::Team::POWERLIFTERS == sniper.team) ? mtm::Team::CROSSFITTERS : mtm::Team::POWERLIFTERS;
    EXPECT_EQ(1, sniper.getAttackCost(&enemy));
    EXPECT_EQ(0, sniper.getAreaOfEffectRadius());
}

TEST_F(SniperFixture, AttackSingleTarget) {
    EXPECT_EQ(0, sniper.getAreaOfEffectRadius());
}
