#ifndef EXAM_DETAILS_H
#define EXAM_DETAILS_H

#include <exception>
#include <string>
#include <iosfwd>

namespace mtm {
    // a class for the details of an exam
    class ExamDetails final {
        // the number serving as the id of the course in which there is a test
        using CourseNumber = int;
        // the month of the test
        using Month = int;
        // the day of the test
        using Day = int;
        // the hour of the test
        using Hour = float;
        // the duration of the test
        using Duration = float;
        // the link to access the test
        using Link = std::string;
    public:
        /**
         * ExamDetails - a constructor
         * @param course_number - the number serving as the id of the course in which there is a test
         * @param month - the month of the test
         * @param day - the day of the test
         * @param hour - the hour of the test
         * @param duration - the duration of the test
         * @param link -the link to access the test
         *
         * @return - an ExamDetails
         */
        ExamDetails(CourseNumber course_number, Month month, Day day,
                    Hour hour, Duration duration, const Link& link = "");
        /**
         *  ~ExamDetails - a default destructor
         */
        ~ExamDetails() = default;
        /**
         * ExamDetails - a default copy constructor
         *
         * @param exam_details - the exam details to be copied
         */
        ExamDetails(const ExamDetails& exam_details) = default;
        /**
         * operator= - a default copy constructor that returns a reference
         *
         * @param exam_details - the exam details to be copied
         *
         * @return - a reference of the copy
         */
        ExamDetails& operator=(const ExamDetails& exam_details) = default;
        /**
         * @return - the link of the exam details
         */
        virtual const ExamDetails::Link& getLink() const;
        /**
         * setLink - changes the link to the exam
         *
         * @param link - the link to be set as the exam link
         */
        void setLink(const ExamDetails::Link& link);

        static ExamDetails makeMatamExam();

        /**
         * operator- - gets the time difference in days between two exams
         *
         * @param left - the exam from which the time will be measured
         * @param right - the exam to which the time will be measured
         *
         * @return - the difference in days between left and right
         */
        friend Day operator-(const ExamDetails& left, const ExamDetails& right)
        {
            return MONTH_SIZE * (left.month - right.month) + (left.day - right.day);
        }

        /**
         * operator< - checks which exam is after the other
         *
         * @param left - one of the exams to be compared
         * @param right - one of the exams to be compared
         *
         * @return false if right is after left and true otherwise
         */
        friend bool operator<(const ExamDetails& left, const ExamDetails& right)
        {
            return left - right < 0;
        }

        /**
         * operator<< - printing operator
         *
         * @param out - where the output will be stored
         * @param exam_details - the object which will be printed
         *
         */
        friend std::ostream& operator<<(std::ostream& out, const ExamDetails& exam_details)
        {
            return exam_details.print(out);
        }

        // exception class for ExamDetails to be used when an invalid date is given
        class InvalidDateException final : public std::exception {
            virtual const char* what() const noexcept override final
            {
                return "Invalid date";
            }
        };

        // exception class for ExamDetails to be used when an invalid time is given
        class InvalidTimeException final : public std::exception {
            virtual const char* what() const noexcept override final
            {
                return "Invalid time";
            }
        };

        // exception class for ExamDetails to be used when an invalid argument besides the previous two is given
        class InvalidArgsException final : public std::exception {
            virtual const char* what() const noexcept override final
            {
                return "Invalid args";
            }
        };

    private:
        // constants for the example of matam course
        static const CourseNumber MATAM_COURSE_NUMBER;
        static const Link MATAM_LINK;
        static const int YEAR_SIZE;
        static const int MONTH_SIZE;

        // function to verify if the hour is valid
        static const Hour& verifyHour(const Hour& hour);
        // function to verify if the duration is valid
        static const Duration& verifyDuration(const Duration& duration);
        static std::string showTime(const Hour& hour);

        // a helper to let operator<< be implemented as a hidden friend
        std::ostream &print(std::ostream &out) const;

        CourseNumber course_number;
        Month month;
        Day day;
        Hour hour;
        Duration duration;
        Link link;
    };
}

#endif // EXAM_DETAILS_H
