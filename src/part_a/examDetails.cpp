#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include "examDetails.h"

using mtm::ExamDetails;

const ExamDetails::CourseNumber ExamDetails::MATAM_COURSE_NUMBER = 234124;
const ExamDetails::Link ExamDetails::MATAM_LINK = "https://tinyurl.com/59hzps6m";
const int ExamDetails::YEAR_SIZE = 12;
const int ExamDetails::MONTH_SIZE = 30;

template<typename Error, typename Comparable>
static Comparable verifyGreaterThan(const Comparable& low, const Comparable& value);

template<typename Error, typename Comparable>
static Comparable verifyBounds(const Comparable& low, const Comparable& high, const Comparable& value);

ExamDetails::ExamDetails(const CourseNumber course_number, const Month month,
                         const Day day, const Hour hour, const Duration duration,
                         const Link &link)
        : course_number{verifyGreaterThan<InvalidArgsException>(0, course_number)}
        , month{verifyBounds<InvalidDateException>(1, ExamDetails::YEAR_SIZE, month)}
        , day{verifyBounds<InvalidDateException>(1, ExamDetails::MONTH_SIZE, day)}
        , hour{verifyHour(hour)}
        , duration{verifyDuration(duration)}
        , link{link}
{

}

const ExamDetails::Link& ExamDetails::getLink() const
{
    return link;
}

void ExamDetails::setLink(const Link& new_link)
{
    link = new_link;
}

ExamDetails ExamDetails::makeMatamExam()
{
    return ExamDetails{MATAM_COURSE_NUMBER, 7, 28, 13, 3, MATAM_LINK};
}

const ExamDetails::Hour& ExamDetails::verifyHour(const Hour& hour)
{
    verifyBounds<InvalidTimeException, Hour>(0, 24, hour);
    double integral_part = 0;
    const double fractional = modf(hour, &integral_part);
    if ((0 == fractional) || (0.5 == fractional)) {
        return hour;
    }
    throw InvalidTimeException{};
}

const ExamDetails::Duration& ExamDetails::verifyDuration(const ExamDetails::Duration& duration)
{
    verifyGreaterThan<InvalidArgsException, ExamDetails::Duration>(0, duration);
    double integral_part = 0;
    if (0 == modf(duration, &integral_part)) {
        return duration;
    }
    throw InvalidArgsException{};
}

std::string ExamDetails::showTime(const Hour& hour)
{
    double integral_part = 0;
    const double fraction = modf(hour, &integral_part);
    std::ostringstream out;
    out << integral_part << ':' << std::setw(2) << std::setfill('0') << (60 * fraction);
    return out.str();
}

std::ostream& ExamDetails::print(std::ostream& out) const
{
    return out
            << "Course Number: " << course_number
            << "\nTime: " << day << "." << month << " at " << showTime(hour)
            << "\nDuration: " << showTime(duration)
            << "\nZoom Link: " << link << std::endl;
}

template<typename Error, typename Comparable>
static Comparable verifyGreaterThan(const Comparable& low, const Comparable& value)
{
    if (low > value) {
        throw Error{};
    }
    return value;
}

template<typename Error, typename Comparable>
static Comparable verifyBounds(const Comparable& low, const Comparable& high, const Comparable& value)
{
    verifyGreaterThan<Error, Comparable>(low, value);
    verifyGreaterThan<Error, Comparable>(value, high);
    return value;
}
