# Local use instructions:
0. Clone:
```bash
$ git clone https://gitlab.com/intro-to-inline-and-void-pointers-public/ex2-public.git
```
1. Copy your sources to the relevant source directories.
2. Install `ninja-build` to enjoy faster parallel builds (or delete / comment out `./PreLoad.cmake` if you need make).
3. Add your sources
4. Run `./build.sh`.
```diff
$ git diff
diff --git a/CMakeLists.txt b/CMakeLists.txt
index fc74bdb..05c57e0 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -22,11 +22,13 @@ set(SOURCES
         src/sortedList.cpp
         src/examDetails.cpp
         # Add more sources here
+        src/myStuff.cpp
         )

 set(TEST_SOURCES
         src/tests/exam_details_test.cpp
         # Add more sources here
+        src/myNewTest.cpp
         )
```
And you're done :)

You may use
* `./build.sh` to build
* `./test.sh` to run all tests
* `./memtest.sh` to run all tests with valgrind
(These are useful in the shell.
If you use an IDE, after it reloads the cmake project it should let you run tests with a click of a button.
If it doesn't, you probably need to learn it better or switch to a different IDE.)


# Motivation:
1. [Learn in public](https://twitter.com/swyx/status/1009174159690264579?s=19) (the entire thread is highly recommended)
2. [Inspire others to join](https://www.youtube.com/watch?v=fW8amMCVAJQ)


# Requirements:
1. gcc
2. valgrind
3. ninja (optional, disable PreLoad.cmake to use normal "Unix Makefile")
4. pandoc (optional, for markdown-to-pdf conversion)
5. rsync (optional, for easy sync with csl3)


See previous [Merge Requests](https://gitlab.com/intro-to-inline-and-void-pointers-public/ex2-tests/-/merge_requests) to see how to add your own tests for everyone else's benefit.
