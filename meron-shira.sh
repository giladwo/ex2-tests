#!/usr/bin/env bash

# ./test.sh
./build.sh && diff -s --context=5 src/tests/meron-shira-tests/expected_v5.txt <(build/meron-shira-test) | bat -l diff
# ./build.sh && diff -y --left-column src/tests/meron-shira-tests/expected_v5.txt <(build/meron-shira-test) | bat
